// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example



// A rectangular box
class Box {

  // We need to keep track of a Body and a width and height
  Body body;
  float w;
  float h;
  PShape myShape;
  int id;
  int interval = 40;
  int letter;

  color theColor;

  // Constructor
  Box(float _w, float _h, PShape _myShape, int id, int _letter, color _theColor) {
    if (_letter == 87)
    {
      w = _w + 40;
    } else if (_letter == 77){
      w = _w + 20;
    } else {
      w = _w;
    }
    h= _h;
    myShape = _myShape;
    letter = _letter;
    theColor = _theColor;

    // Add the box to the box2d world
    makeBody(new Vec2(width/2, dropZone-(interval*id)), w, h);
  }

  // This function removes the particle from the box2d world
  void killBody() {
    box2d.destroyBody(body);
    
  }

  boolean contains(float x, float y) {
    Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }

  // Is the particle ready for deletion?
  boolean done() {
    // Let's find the screen position of the particle
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Is it off the bottom of the screen?
    if (pos.y > height+100) {
      killBody();
      return true;
    }
    return false;
  }

  // Drawing the box
  void display() {

    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();

    offscreen.rectMode(CENTER);
    offscreen.pushMatrix();
    offscreen.translate(pos.x, pos.y);
    offscreen.rotate(-a);
    //fill(175);
    //stroke(0);
    offscreen.noStroke();

    if (letter == -1)
    {
      offscreen.shape(myShape, -w/2, -h/2, w, h);
    } else {
      offscreen.fill(theColor);
      offscreen.rect(0, 0, w, h);
      offscreen.fill(0);
      offscreen.textFont(f);
      offscreen.textAlign(CENTER, CENTER);
      offscreen.textSize(90);
      offscreen.text(char(letter), 0, -15);
    }
    offscreen.popMatrix();
  }

  // This function adds the rectangle to the box2d world
  void makeBody(Vec2 center, float w_, float h_) {

    // Define a polygon (this is what we use for a rectangle)
    PolygonShape sd = new PolygonShape();
    float box2dW = box2d.scalarPixelsToWorld(w_/2);
    float box2dH = box2d.scalarPixelsToWorld(h_/2);
    sd.setAsBox(box2dW, box2dH);

    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = sd;
    //Q Parameters that affect physics
    fd.density = 2;
    fd.friction = 0.5;
    fd.restitution = 0.2;

    /*
    //FLYER A6
     float theScale = 1.3;
     size(594, 840)
     gravity -22
     fd.density = 1;
     fd.friction = 0.5;
     fd.restitution = 0.1;
     
     //CADROBUS
     float theScale = 1.65;
     int dropZone = -900;
     size(435, 1000)
     gravity -20
     fd.density = 2;
     fd.friction = 0.6;
     fd.restitution = 0.4;
     */

    // Define the body and make it from the shape
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(center));

    body = box2d.createBody(bd);
    body.createFixture(fd);

    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(random(-5, 5), random(2, 5)));
    body.setAngularVelocity(random(-5, 5));
  }

  PShape getMyShape()
  {
    return myShape;
  }
}