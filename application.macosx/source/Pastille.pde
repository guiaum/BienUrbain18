// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// A rectangular box
class Pastille{

  // We need to keep track of a Body and a width and height
  Body body;
  float r;
  PImage myShape;
  int id;
  int interval = 20;
  
  // Constructor
  Pastille(float _r, PImage _myShape, int id) {
   r = _r;
   myShape = _myShape;
   
    // Add the box to the box2d world
    makeBody(new Vec2(width/2, dropZone-(interval*id)), r);
  }

  // This function removes the particle from the box2d world
  void killBody() {
    box2d.destroyBody(body);
    println("ciao dada");
    dadaCreated = false;
  }
  
  boolean contains(float x, float y) {
    Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }
  

  // Drawing the box
  void display() {
    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();

    ellipseMode(CENTER);
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    //fill(175);
    //stroke(255,0,0);
    
    //ellipse(0, 0, r, r);
    image(myShape, -r/2, -r/2, r, r);
    popMatrix();
  }

  // This function adds the rectangle to the box2d world
  void makeBody(Vec2 center, float r) {
    
    // Define a polygon (this is what we use for a rectangle)
    CircleShape cs = new CircleShape();
    cs.m_radius = box2d.scalarPixelsToWorld(r/2);

    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = cs;
    //Q Parameters that affect physics
    fd.density = 2;
    fd.friction = 0.6;
    fd.restitution = 0.4;
    
    // Define the body and make it from the shape
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(center));

    body = box2d.createBody(bd);
    body.createFixture(fd);

    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(random(-5, 5), random(2, 5)));
    body.setAngularVelocity(random(-5, 5));
  }
  
  int theY()
  {
   Vec2 pos = box2d.getBodyPixelCoord(body);
    
   return int(pos.y); 
  }

}