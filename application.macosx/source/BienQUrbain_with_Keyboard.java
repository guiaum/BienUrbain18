import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import shiffman.box2d.*; 
import org.jbox2d.common.*; 
import org.jbox2d.dynamics.joints.*; 
import org.jbox2d.collision.shapes.*; 
import org.jbox2d.collision.shapes.Shape; 
import org.jbox2d.common.*; 
import org.jbox2d.dynamics.*; 
import processing.pdf.*; 
import java.util.Collections; 
import java.util.Random; 
import java.util.Arrays; 
import deadpixel.keystone.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class BienQUrbain_with_Keyboard extends PApplet {














//PARAMS
float theScale = 1.90f;
int dropZone = -150;
int trapDiv = 3;

//COLORS
int[] colors = 
  { 
  0xffFF198c, 
  0xff00a06e, 
  0xff7214aa, 
  0xffffcc00, 
};

//DADA
String pathToMyPicDada = "dada.png";
PImage MyPicDada;
Pastille p;
boolean dada = true;
boolean dadaCreated = false;


// Ici faut mettre soit false soit true
// Juste pour visualiser la trappe (mettre false pour cacher)
boolean debug = true;

// A reference to our box2d world
Box2DProcessing box2d;

// A list we'll use to track fixed objects
ArrayList<Boundary> boundaries;
ArrayList<Boundary> boundariesBottom;
// A list for all of our rectangles
ArrayList<Box> boxes;
// A list of all our images
ArrayList<PShape> artists;

Box selectedBox;
int selectedIndex;
boolean savePDF = false;
boolean recording = false;
PFont f;

//HELPERS
int incrementalIndex = 0;
int incrementalId = 0;
int incrementalIndexColor = 0;
int incrementalIndexArtists = 0;

long lastLetter = 0;
int delayBetweenDelay = 10;

// The Spring that will attach to the box from the mouse
Spring spring;
SpringPastille springPastille;

// Combos
boolean shift = false;
boolean shiftBottom = false;
boolean[] keysBU =new boolean[2];
boolean[] keysDADA =new boolean[2];
//BU
boolean buStarted = false;
int lastBUdropped;
int indexCharBU = 0;
char[] buArray = {'N', 'I', 'A', 'B', 'R', 'U', 'N', 'E', 'I', 'B'};

String[] filenames;

//Automation
long lastKeyPressed;
long pauseTime = 60000;
boolean artistsAlreadyCreated = false;

//Keystone helper for projection
Keystone ks;
CornerPinSurface surface;

PGraphics offscreen;


public void setup() {
  //Q
  
  

  //Keystone
  ks = new Keystone(this);
  surface = ks.createCornerPinSurface(1024, 768, 20);
  offscreen = createGraphics(1024, 768, P3D);
  
  // Initialize box2d physics and create the world
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  // We are setting a custom gravity
  //Q
  box2d.setGravity(0, -20);

  // Create ArrayLists	
  boxes = new ArrayList<Box>();
  boundaries = new ArrayList<Boundary>();
  boundariesBottom = new ArrayList<Boundary>();
  artists = new ArrayList<PShape>();

  // Make the spring (it doesn't really get initialized until the mouse is clicked)
  spring = new Spring();
  springPastille = new SpringPastille();

  // Add a bunch of fixed boundaries
  boundaries.add(new Boundary(-5, 0, 10, height*4));
  boundaries.add(new Boundary(width+5, 0, 10, height*4));
  createBoundariesBottom();

  //Hello Dada
  MyPicDada = loadImage(pathToMyPicDada);

  f = createFont("GTHaptik-Black", 90);
  offscreen.textFont(f);
  offscreen.textAlign(CENTER, CENTER);

  // COMBO
  keysBU[0] = keysBU[1] = false;
  keysDADA[0] = keysDADA[1] = false;

  String path = sketchPath()+"/data/artists/";

  println("Listing all filenames in a directory: ");
  filenames = listFileNames(path);
  for (int i = 0; i<filenames.length; i++)
  {
    if (filenames[i].charAt(0) == '.') continue;
    else if (filenames[i].toLowerCase().endsWith(".svg")) {
      PShape MyPic = loadShape("artists/"+filenames[i]);
      artists.add(MyPic);
    }
  }
}

public void draw() {
  PVector surfaceMouse = surface.getTransformedMouse();
  
  offscreen.beginDraw();
  
  
  offscreen.background(255);

  // We must always step through time!
  box2d.step();
  // Always alert the spring to the new mouse location
  spring.update(mouseX, mouseY);

  if (debug)
  {
    // Display all the boundaries
    for (Boundary wall : boundariesBottom) {
      wall.display();
    }
  }
  if (savePDF == true) {
    beginRecord(PDF, "BienQurbain" + frameCount + ".pdf");
  }

  // Display all the boxes
  for (Box b : boxes) {
    b.display();
  }

  if (dada && dadaCreated)
  {
    p.display();
    springPastille.update(mouseX, mouseY);
    if (p.theY() > height +150)
    {
      p.killBody();
      dadaCreated = false;
    }
  }

  if (recording) {
    saveFrame("output/frames####.png");
  }
  if (savePDF == true) {
    endRecord();
    savePDF = false;
  }

  // Boxes that leave the screen, we delete them
  // (note they have to be deleted from both the box2d world and our list

  for (int i = boxes.size()-1; i >= 0; i--) {
    Box b = boxes.get(i);
    if (b.done()) {
      boxes.remove(i);
    }
  }
  //println (boxes.size());

  if (boxes.size() == 0 && boundariesBottom.size()== 0 && dadaCreated == false)
  {
    createBoundariesBottom();
  }


  if (keysBU[0] == true &&  keysBU[1]== true)
  {
    buStarted = true;
    lastBUdropped = millis();
    indexCharBU = 0;
    keysBU[0] = false;
    keysBU[1] = false;
  }
  
  if (buStarted == true)
  {
   bu(); 
  }

  if (keysDADA[0] == true &&  keysDADA[1]== true && dadaCreated == false)
  {
    dada();
    keysDADA[0] = false;
    keysDADA[1] = false;
  }

  if (millis() - lastKeyPressed > pauseTime && artistsAlreadyCreated == false)
  {
    allArtists();
    buStarted = true;
    lastBUdropped = millis();
    indexCharBU = 0;
    artistsAlreadyCreated = true;
  }

  if (millis() - lastKeyPressed > pauseTime*2 && artistsAlreadyCreated == true)
  {
    while (boundariesBottom.size()>0)
    {
      boundariesBottom.get(boundariesBottom.size()-1).killBody();
      boundariesBottom.remove(boundariesBottom.size()-1);
      incrementalId = 0;
      lastKeyPressed = millis();
      artistsAlreadyCreated = false;
    }
  }
  offscreen.endDraw();
  background(0);
 
  // render the scene, transformed using the corner pin surface
  surface.render(offscreen);
}

// When the mouse is released we're done with the spring
public void mouseReleased() {
  spring.destroy();
  if (dada && dadaCreated)
  {
    springPastille.destroy();
  }
  //box2d.setGravity(0, -20);
}

// When the mouse is pressed we. . .
public void mousePressed() {
  for (int i = 0; i<boxes.size(); i++)
  {
    Box box = boxes.get(i);
    // Check to see if the mouse was clicked on the box
    if (box.contains(mouseX, mouseY)) {
      // And if so, bind the mouse location to the box with a spring
      spring.bind(mouseX, mouseY, box);
      selectedBox = box;
      selectedIndex = i;
    }
  }
  // GINKO
  if (dada && dadaCreated)
  {
    if (p.contains(mouseX, mouseY)) {
      springPastille.bind(mouseX, mouseY, p);
    }
  }
}





public void allArtists()
{
  for (int i = 0; i<artists.size(); i++)
  {
    PShape MyPic = artists.get(i);
    Box p =  new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, i, -1, 0);
    boxes.add(p);
  }
}

public void oneArtist(int index)
{

  PShape MyPic = artists.get(index);
  Box p =  new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, index, -1, 0);
  boxes.add(p);
}

public void dada()
{
  p = new Pastille(MyPicDada.width/theScale, MyPicDada, 0);
  dadaCreated = true;
}


public void bu()
{
  if (millis() - lastBUdropped > 300)
    {
      createLetter(buArray[indexCharBU]);
      println (buArray[indexCharBU] + "dropped at "+ lastBUdropped);
      lastBUdropped = millis();
      indexCharBU ++;
      if (indexCharBU == buArray.length)
      {
        buStarted = false;
      }
    }
}

public void keyPressed() {
  lastKeyPressed = millis();
  artistsAlreadyCreated = true;
  int keyIndex = -1;
  if ( key >= 'a' && key <= 'z' && shift == false) {
    keyIndex = key - (97-65)  ;
    println (PApplet.parseInt(key), keyIndex);
    createLetter(keyIndex);
    if (incrementalIndexColor%3 == 0)
    {
      oneArtist(incrementalIndexArtists);
      if (incrementalIndexArtists < artists.size()-1)
      {
        incrementalIndexArtists ++;
      } else {
        incrementalIndexArtists = 0;
      }
    }
  } else 
  if (key == ' ')
  {
    if (boundariesBottom.size()>0)
    {
      boundariesBottom.get(boundariesBottom.size()-1).killBody();
      boundariesBottom.remove(boundariesBottom.size()-1);
      incrementalId = 0;
    }
  }
  if (key == CODED && keyCode == UP)shift = true;
  if (key == 'b' && shift == true)keysBU[0] = true;
  if (key == 'u' && shift == true)keysBU[1] = true;
  if (key == 'd' && shift == true)keysDADA[0] = true;
  if (key == 'a' && shift == true)keysDADA[1] = true;
  if (key == CODED && keyCode == DOWN)shiftBottom = true;
  if (key == 'c' && shiftBottom == true)  ks.toggleCalibration();
  if (key == 's' && shiftBottom == true) ks.load();
  if (key == 'l' && shiftBottom == true) ks.save();
    
  
}


public void keyReleased()
{
  if (key == CODED && keyCode == UP)shift = false;
  if (key == CODED && keyCode == DOWN)shiftBottom = false;
  if (key == 'b')keysBU[0] = false;
  if (key == 'u')keysBU[1] = false;
  if (key == 'd' && shift == true)keysDADA[0] = false;
  if (key == 'a' && shift == true)keysDADA[1] = false;
} 

public void createLetter(int keyIndex)
{
  if (millis()-lastLetter> delayBetweenDelay)
  {
    Box p = new Box(170/theScale, 245/theScale, null, incrementalId, keyIndex, colors[incrementalIndexColor%colors.length]);
    println (keyIndex);
    boxes.add(p);
    incrementalIndexColor++;
    incrementalId ++;

    lastLetter = millis();
  }
}

// This function returns all the files in a directory as an array of Strings  
public String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    //Arrays.sort(names);
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

public void shuffleArray(String[] array) {

  // with code from WikiPedia; Fisher\u2013Yates shuffle 
  //@ <a href="http://en.wikipedia.org/wiki/Fisher" target="_blank" rel="nofollow">http://en.wikipedia.org/wiki/Fisher</a>\u2013Yates_shuffle

  Random rng = new Random();

  // i is the number of items remaining to be shuffled.
  for (int i = array.length; i > 1; i--) {

    // Pick a random element to swap with the i-th element.
    int j = rng.nextInt(i);  // 0 <= j <= i-1 (0-based array)

    // Swap array elements.
    String tmp = array[j];
    array[j] = array[i-1];
    array[i-1] = tmp;
  }
}

public void createBoundariesBottom()
{
  // Cr\u00e9ation de boundaries pour la trappe
  for (int i = 0; i<trapDiv; i++)
  {
    int widthBoundary = (width/trapDiv)+1;
    int originX = widthBoundary*i + (widthBoundary/2);
    int originY = height-13;
    boundariesBottom.add(new Boundary(originX, originY, widthBoundary, 25));
  }
  Collections.shuffle(boundariesBottom);
}
// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2012
// Box2DProcessing example

// A fixed boundary class

class Boundary {

  // A boundary is a simple rectangle with x,y,width,and height
  float x;
  float y;
  float w;
  float h;
  
  // But we also have to make a body for box2d to know about it
  Body b;

  Boundary(float x_,float y_, float w_, float h_) {
    x = x_;
    y = y_;
    w = w_;
    h = h_;

    // Define the polygon
    PolygonShape sd = new PolygonShape();
    // Figure out the box2d coordinates
    float box2dW = box2d.scalarPixelsToWorld(w/2);
    float box2dH = box2d.scalarPixelsToWorld(h/2);
    // We're just a box
    sd.setAsBox(box2dW, box2dH);


    // Create the body
    BodyDef bd = new BodyDef();
    bd.type = BodyType.STATIC;
    bd.position.set(box2d.coordPixelsToWorld(x,y));
    b = box2d.createBody(bd);
    
    // Attached the shape to the body using a Fixture
    b.createFixture(sd,1);
  }

  // Draw the boundary, if it were at an angle we'd have to do something fancier
  public void display() {
    offscreen.fill(0);
    offscreen.stroke(0);
    offscreen.rectMode(CENTER);
    offscreen.rect(x,y,w,h);
  }
  
  // This function removes the particle from the box2d world
  public void killBody() {
    box2d.destroyBody(b);
  }
}
// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example



// A rectangular box
class Box {

  // We need to keep track of a Body and a width and height
  Body body;
  float w;
  float h;
  PShape myShape;
  int id;
  int interval = 40;
  int letter;

  int theColor;

  // Constructor
  Box(float _w, float _h, PShape _myShape, int id, int _letter, int _theColor) {
    if (_letter == 87)
    {
      w = _w + 40;
    } else if (_letter == 77){
      w = _w + 20;
    } else {
      w = _w;
    }
    h= _h;
    myShape = _myShape;
    letter = _letter;
    theColor = _theColor;

    // Add the box to the box2d world
    makeBody(new Vec2(width/2, dropZone-(interval*id)), w, h);
  }

  // This function removes the particle from the box2d world
  public void killBody() {
    box2d.destroyBody(body);
    
  }

  public boolean contains(float x, float y) {
    Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }

  // Is the particle ready for deletion?
  public boolean done() {
    // Let's find the screen position of the particle
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Is it off the bottom of the screen?
    if (pos.y > height+100) {
      killBody();
      return true;
    }
    return false;
  }

  // Drawing the box
  public void display() {

    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();

    offscreen.rectMode(CENTER);
    offscreen.pushMatrix();
    offscreen.translate(pos.x, pos.y);
    offscreen.rotate(-a);
    //fill(175);
    //stroke(0);
    offscreen.noStroke();

    if (letter == -1)
    {
      offscreen.shape(myShape, -w/2, -h/2, w, h);
    } else {
      offscreen.fill(theColor);
      offscreen.rect(0, 0, w, h);
      offscreen.fill(0);
      offscreen.textFont(f);
      offscreen.textAlign(CENTER, CENTER);
      offscreen.textSize(90);
      offscreen.text(PApplet.parseChar(letter), 0, -15);
    }
    offscreen.popMatrix();
  }

  // This function adds the rectangle to the box2d world
  public void makeBody(Vec2 center, float w_, float h_) {

    // Define a polygon (this is what we use for a rectangle)
    PolygonShape sd = new PolygonShape();
    float box2dW = box2d.scalarPixelsToWorld(w_/2);
    float box2dH = box2d.scalarPixelsToWorld(h_/2);
    sd.setAsBox(box2dW, box2dH);

    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = sd;
    //Q Parameters that affect physics
    fd.density = 2;
    fd.friction = 0.5f;
    fd.restitution = 0.2f;

    /*
    //FLYER A6
     float theScale = 1.3;
     size(594, 840)
     gravity -22
     fd.density = 1;
     fd.friction = 0.5;
     fd.restitution = 0.1;
     
     //CADROBUS
     float theScale = 1.65;
     int dropZone = -900;
     size(435, 1000)
     gravity -20
     fd.density = 2;
     fd.friction = 0.6;
     fd.restitution = 0.4;
     */

    // Define the body and make it from the shape
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(center));

    body = box2d.createBody(bd);
    body.createFixture(fd);

    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(random(-5, 5), random(2, 5)));
    body.setAngularVelocity(random(-5, 5));
  }

  public PShape getMyShape()
  {
    return myShape;
  }
}
// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// A rectangular box
class Pastille{

  // We need to keep track of a Body and a width and height
  Body body;
  float r;
  PImage myShape;
  int id;
  int interval = 20;
  
  // Constructor
  Pastille(float _r, PImage _myShape, int id) {
   r = _r;
   myShape = _myShape;
   
    // Add the box to the box2d world
    makeBody(new Vec2(width/2, dropZone-(interval*id)), r);
  }

  // This function removes the particle from the box2d world
  public void killBody() {
    box2d.destroyBody(body);
    println("ciao dada");
    dadaCreated = false;
  }
  
  public boolean contains(float x, float y) {
    Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }
  

  // Drawing the box
  public void display() {
    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);
    // Get its angle of rotation
    float a = body.getAngle();

    ellipseMode(CENTER);
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(-a);
    //fill(175);
    //stroke(255,0,0);
    
    //ellipse(0, 0, r, r);
    image(myShape, -r/2, -r/2, r, r);
    popMatrix();
  }

  // This function adds the rectangle to the box2d world
  public void makeBody(Vec2 center, float r) {
    
    // Define a polygon (this is what we use for a rectangle)
    CircleShape cs = new CircleShape();
    cs.m_radius = box2d.scalarPixelsToWorld(r/2);

    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = cs;
    //Q Parameters that affect physics
    fd.density = 2;
    fd.friction = 0.6f;
    fd.restitution = 0.4f;
    
    // Define the body and make it from the shape
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;
    bd.position.set(box2d.coordPixelsToWorld(center));

    body = box2d.createBody(bd);
    body.createFixture(fd);

    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(random(-5, 5), random(2, 5)));
    body.setAngularVelocity(random(-5, 5));
  }
  
  public int theY()
  {
   Vec2 pos = box2d.getBodyPixelCoord(body);
    
   return PApplet.parseInt(pos.y); 
  }

}
// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// Class to describe the spring joint (displayed as a line)

class Spring {

  // This is the box2d object we need to create
  MouseJoint mouseJoint;

  Spring() {
    // At first it doesn't exist
    mouseJoint = null;
  }

  // If it exists we set its target to the mouse location 
  public void update(float x, float y) {
    if (mouseJoint != null) {
      // Always convert to world coordinates!
      Vec2 mouseWorld = box2d.coordPixelsToWorld(x,y);
      mouseJoint.setTarget(mouseWorld);
    }
  }

  public void display() {
    if (mouseJoint != null) {
      // We can get the two anchor points
      Vec2 v1 = new Vec2(0,0);
      mouseJoint.getAnchorA(v1);
      Vec2 v2 = new Vec2(0,0);
      mouseJoint.getAnchorB(v2);
      // Convert them to screen coordinates
      v1 = box2d.coordWorldToPixels(v1);
      v2 = box2d.coordWorldToPixels(v2);
      // And just draw a line
      stroke(0);
      strokeWeight(1);
      line(v1.x,v1.y,v2.x,v2.y);
    }
  }


  // This is the key function where
  // we attach the spring to an x,y location
  // and the Box object's location
  public void bind(float x, float y, Box box) {
    // Define the joint
    MouseJointDef md = new MouseJointDef();
    // Body A is just a fake ground body for simplicity (there isn't anything at the mouse)
    md.bodyA = box2d.getGroundBody();
    // Body 2 is the box's boxy
    md.bodyB = box.body;
    // Get the mouse location in world coordinates
    Vec2 mp = box2d.coordPixelsToWorld(x,y);
    // And that's the target
    md.target.set(mp);
    // Some stuff about how strong and bouncy the spring should be
    md.maxForce = 1000.0f * box.body.m_mass;
    md.frequencyHz = 5.0f;
    md.dampingRatio = 0.9f;

    // Make the joint!
    mouseJoint = (MouseJoint) box2d.world.createJoint(md);
  }

  public void destroy() {
    // We can get rid of the joint when the mouse is released
    if (mouseJoint != null) {
      box2d.world.destroyJoint(mouseJoint);
      mouseJoint = null;
    }
  }

}
// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// Class to describe the spring joint (displayed as a line)

class SpringPastille {

  // This is the box2d object we need to create
  MouseJoint mouseJoint;

  SpringPastille() {
    // At first it doesn't exist
    mouseJoint = null;
  }

  // If it exists we set its target to the mouse location 
  public void update(float x, float y) {
    if (mouseJoint != null) {
      // Always convert to world coordinates!
      Vec2 mouseWorld = box2d.coordPixelsToWorld(x,y);
      mouseJoint.setTarget(mouseWorld);
    }
  }

  public void display() {
    if (mouseJoint != null) {
      // We can get the two anchor points
      Vec2 v1 = new Vec2(0,0);
      mouseJoint.getAnchorA(v1);
      Vec2 v2 = new Vec2(0,0);
      mouseJoint.getAnchorB(v2);
      // Convert them to screen coordinates
      v1 = box2d.coordWorldToPixels(v1);
      v2 = box2d.coordWorldToPixels(v2);
      // And just draw a line
      stroke(0);
      strokeWeight(1);
      line(v1.x,v1.y,v2.x,v2.y);
    }
  }


  // This is the key function where
  // we attach the spring to an x,y location
  // and the Box object's location
  public void bind(float x, float y, Pastille p) {
    // Define the joint
    MouseJointDef md = new MouseJointDef();
    // Body A is just a fake ground body for simplicity (there isn't anything at the mouse)
    md.bodyA = box2d.getGroundBody();
    // Body 2 is the box's boxy
    md.bodyB = p.body;
    // Get the mouse location in world coordinates
    Vec2 mp = box2d.coordPixelsToWorld(x,y);
    // And that's the target
    md.target.set(mp);
    // Some stuff about how strong and bouncy the spring should be
    md.maxForce = 1000.0f * p.body.m_mass;
    md.frequencyHz = 5.0f;
    md.dampingRatio = 0.9f;

    // Make the joint!
    mouseJoint = (MouseJoint) box2d.world.createJoint(md);
  }

  public void destroy() {
    // We can get rid of the joint when the mouse is released
    if (mouseJoint != null) {
      box2d.world.destroyJoint(mouseJoint);
      mouseJoint = null;
    }
  }

}
  public void settings() {  size(1024, 768, P3D);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "BienQUrbain_with_Keyboard" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
