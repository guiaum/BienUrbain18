// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2011
// Box2DProcessing example

// Basic example of falling rectangles

import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import processing.pdf.*;
import java.util.Collections;
import java.util.Random;
import java.util.Arrays;


//String pathToMyPicGinko = "../ginko/ginko.svg";
//PShape MyPicGinko;
//Pastille p;
boolean ginko = false;
boolean ginkoCreated = false;

//Q
float theScale = 1.65;
int dropZone = -900;
int trapDiv = 3;

// Ici faut mettre soit false soit true
// Juste pour visualiser la trappe (mettre false pour cacher)
boolean debug = false;
// mélange les blocs quand c'est true
boolean blocAleatoires = false;

// A reference to our box2d world
Box2DProcessing box2d;

// A list we'll use to track fixed objects
ArrayList<Boundary> boundaries;
ArrayList<Boundary> boundariesBottom;
// A list for all of our rectangles
ArrayList<Box> boxes;
// A list of all our images
ArrayList<PShape> pics;

Box selectedBox;
int selectedIndex;

boolean savePDF = false;
boolean recording = false;



int incrementalIndex = 0;

// The Spring that will attach to the box from the mouse
Spring spring;
SpringPastille springPastille;

void setup() {
  //Q
  size(435, 1000);
  smooth();

  // Initialize box2d physics and create the world
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  // We are setting a custom gravity
  //Q
  box2d.setGravity(0, -20);

  // Create ArrayLists	
  boxes = new ArrayList<Box>();
  boundaries = new ArrayList<Boundary>();
  boundariesBottom = new ArrayList<Boundary>();
  pics = new ArrayList<PShape>();

  // Make the spring (it doesn't really get initialized until the mouse is clicked)
  spring = new Spring();
  springPastille = new SpringPastille();

  // Add a bunch of fixed boundaries
  boundaries.add(new Boundary(-5, 0, 10, height*4));
  boundaries.add(new Boundary(width+5, 0, 10, height*4));

  // Création de boundaries pour la trappe
  for (int i = 0; i<trapDiv; i++)
  {
    int widthBoundary = width/trapDiv;
    int originX = widthBoundary*i + (widthBoundary/2);
    int originY = height+5;
    if (debug) originY = height-5;
    boundariesBottom.add(new Boundary(originX, originY, widthBoundary, 10));
  }
  Collections.shuffle(boundariesBottom);
  /*
    MyPicGinko = loadShape(pathToMyPicGinko);
   println (MyPicGinko.width);
   */
  //createBoxes();
}

void draw() {
  background(255);

  // We must always step through time!
  box2d.step();
  // Always alert the spring to the new mouse location
  spring.update(mouseX, mouseY);

  if (debug)
  {
    // Display all the boundaries
    for (Boundary wall : boundariesBottom) {
      wall.display();
    }
  }
  if (savePDF == true) {
    beginRecord(PDF, "BienQurbain" + frameCount + ".pdf");
  }

  // Display all the boxes
  for (Box b : boxes) {
    b.display();
  }
  /*
  if (ginko && ginkoCreated)
  {
    p.display();
    springPastille.update(mouseX, mouseY);
  }
  */
  if (recording) {
    saveFrame("output/frames####.png");
  }
  if (savePDF == true) {
    endRecord();
    savePDF = false;
  }

  // Boxes that leave the screen, we delete them
  // (note they have to be deleted from both the box2d world and our list
  /*
  for (int i = boxes.size()-1; i >= 0; i--) {
   Box b = boxes.get(i);
   if (b.done()) {
   boxes.remove(i);
   }
   }
   */
}

// When the mouse is released we're done with the spring
void mouseReleased() {
  spring.destroy();
  if (ginko && ginkoCreated)
  {
    springPastille.destroy();
  }
}

// When the mouse is pressed we. . .
void mousePressed() {
  for (int i = 0; i<boxes.size(); i++)
  {
    Box box = boxes.get(i);
    // Check to see if the mouse was clicked on the box
    if (box.contains(mouseX, mouseY)) {
      // And if so, bind the mouse location to the box with a spring
      spring.bind(mouseX, mouseY, box);
      selectedBox = box;
      selectedIndex = i;
    }
  }
  /*
  // GINKO
  if (ginko && ginkoCreated)
  {
    if (p.contains(mouseX, mouseY)) {
      springPastille.bind(mouseX, mouseY, p);
    }
  }
  */
}


void createBoxes()
{

  String path = sketchPath()+"/data";

  println("Listing all filenames in a directory: ");
  String[] filenames = listFileNames(path);
  printArray(filenames);
  if (blocAleatoires)
  {
    // Si l'option est choisie, on mélange
    shuffleArray(filenames);
    println("Shuffled");
    printArray(filenames);
  }
  for (int i = 0; i<filenames.length; i++)
  {
    if (filenames[i].charAt(0) == '.') continue;
    else if (filenames[i].toLowerCase().endsWith(".svg")) {
      PShape MyPic = loadShape(filenames[i]);
      pics.add(MyPic);
      Box p = new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, i);
      boxes.add(p);
    }
  }

  /*
  // Boxes fall from the top every so often
   if (random(1) < 0.2) {
   Box p = new Box(width/2,30);
   boxes.add(p);
   }
   */
}

void create1Box()
{

  String path = sketchPath()+"/data";

  println("Listing all filenames in a directory: ");
  String[] filenames = listFileNames(path);
  //printArray(filenames);
  if (blocAleatoires)
  {
    // Si l'option est choisie, on mélange
    shuffleArray(filenames);
    println("Shuffled");
    printArray(filenames);
  }
  for (int i = 0; i<filenames.length; i++)
  {
    if (filenames[i].charAt(0) == '.') continue;
    else if (filenames[i].toLowerCase().endsWith(".svg")&& i == incrementalIndex) {
      PShape MyPic = loadShape(filenames[i]);
      pics.add(MyPic);
      Box p = new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, i);
      boxes.add(p);
    }
  }
  incrementalIndex ++;
  println(incrementalIndex);
  /*
  // Boxes fall from the top every so often
   if (random(1) < 0.2) {
   Box p = new Box(width/2,30);
   boxes.add(p);
   }
   */
}

void keyPressed() {
  if (key == 'b')create1Box();
  if (key == ' ')createBoxes();
  if (key == 'p')savePDF = true;
  if (key == 't')
  {
    if (boundariesBottom.size()>0)
    {
      boundariesBottom.get(boundariesBottom.size()-1).killBody();
      boundariesBottom.remove(boundariesBottom.size()-1);
    }
  } else   // If we press r, start or stop recording!
  if (key == 'r' || key == 'R') {
    recording = !recording;
  }
  if (key == 'd' && selectedBox != null)
  {

    // On détruit la dernière boite sélectionnée
    PShape myTempShape = selectedBox.getMyShape();
    selectedBox.killBody();
    boxes.remove(selectedIndex);

    //et on la refabrique au dessus 
    Box p = new Box(myTempShape.width/theScale, myTempShape.height/theScale, myTempShape, selectedIndex );
    boxes.add(p);
  }

  //if (key == 'g' && ginko)addGinko();
}
/*
void addGinko()
{
  p = new Pastille(MyPicGinko.width/theScale, MyPicGinko, 0);
  ginkoCreated = true;
  //boxes.add(p);
}
*/
// This function returns all the files in a directory as an array of Strings  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    //Arrays.sort(names);
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

void shuffleArray(String[] array) {

  // with code from WikiPedia; Fisher–Yates shuffle 
  //@ <a href="http://en.wikipedia.org/wiki/Fisher" target="_blank" rel="nofollow">http://en.wikipedia.org/wiki/Fisher</a>–Yates_shuffle

  Random rng = new Random();

  // i is the number of items remaining to be shuffled.
  for (int i = array.length; i > 1; i--) {

    // Pick a random element to swap with the i-th element.
    int j = rng.nextInt(i);  // 0 <= j <= i-1 (0-based array)

    // Swap array elements.
    String tmp = array[j];
    array[j] = array[i-1];
    array[i-1] = tmp;
  }
}