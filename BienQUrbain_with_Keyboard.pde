import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;
import processing.pdf.*;
import java.util.Collections;
import java.util.Random;
import java.util.Arrays;
import deadpixel.keystone.*;

//PARAMS
float theScale = 1.90;
int dropZone = -150;
int trapDiv = 3;

//COLORS
color[] colors = 
  { 
  #FF198c, 
  #00a06e, 
  #7214aa, 
  #ffcc00, 
};

//DADA
String pathToMyPicDada = "dada.png";
PImage MyPicDada;
Pastille p;
boolean dada = true;
boolean dadaCreated = false;


// Ici faut mettre soit false soit true
// Juste pour visualiser la trappe (mettre false pour cacher)
boolean debug = true;

// A reference to our box2d world
Box2DProcessing box2d;

// A list we'll use to track fixed objects
ArrayList<Boundary> boundaries;
ArrayList<Boundary> boundariesBottom;
// A list for all of our rectangles
ArrayList<Box> boxes;
// A list of all our images
ArrayList<PShape> artists;

Box selectedBox;
int selectedIndex;
boolean savePDF = false;
boolean recording = false;
PFont f;

//HELPERS
int incrementalIndex = 0;
int incrementalId = 0;
int incrementalIndexColor = 0;
int incrementalIndexArtists = 0;

long lastLetter = 0;
int delayBetweenDelay = 10;

// The Spring that will attach to the box from the mouse
Spring spring;
SpringPastille springPastille;

// Combos
boolean shift = false;
boolean shiftBottom = false;
boolean[] keysBU =new boolean[2];
boolean[] keysDADA =new boolean[2];
//BU
boolean buStarted = false;
int lastBUdropped;
int indexCharBU = 0;
char[] buArray = {'N', 'I', 'A', 'B', 'R', 'U', 'N', 'E', 'I', 'B'};

String[] filenames;

//Automation
long lastKeyPressed;
long pauseTime = 60000;
boolean artistsAlreadyCreated = false;

//Keystone helper for projection
Keystone ks;
CornerPinSurface surface;

PGraphics offscreen;


void setup() {
  //Q
  size(1024, 768, P3D);
  smooth();

  //Keystone
  ks = new Keystone(this);
  surface = ks.createCornerPinSurface(1024, 768, 20);
  offscreen = createGraphics(1024, 768, P3D);
  
  // Initialize box2d physics and create the world
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  // We are setting a custom gravity
  //Q
  box2d.setGravity(0, -20);

  // Create ArrayLists	
  boxes = new ArrayList<Box>();
  boundaries = new ArrayList<Boundary>();
  boundariesBottom = new ArrayList<Boundary>();
  artists = new ArrayList<PShape>();

  // Make the spring (it doesn't really get initialized until the mouse is clicked)
  spring = new Spring();
  springPastille = new SpringPastille();

  // Add a bunch of fixed boundaries
  boundaries.add(new Boundary(-5, 0, 10, height*4));
  boundaries.add(new Boundary(width+5, 0, 10, height*4));
  createBoundariesBottom();

  //Hello Dada
  MyPicDada = loadImage(pathToMyPicDada);

  f = createFont("GTHaptik-Black", 90);
  offscreen.textFont(f);
  offscreen.textAlign(CENTER, CENTER);

  // COMBO
  keysBU[0] = keysBU[1] = false;
  keysDADA[0] = keysDADA[1] = false;

  String path = sketchPath()+"/data/artists/";

  println("Listing all filenames in a directory: ");
  filenames = listFileNames(path);
  for (int i = 0; i<filenames.length; i++)
  {
    if (filenames[i].charAt(0) == '.') continue;
    else if (filenames[i].toLowerCase().endsWith(".svg")) {
      PShape MyPic = loadShape("artists/"+filenames[i]);
      artists.add(MyPic);
    }
  }
}

void draw() {
  PVector surfaceMouse = surface.getTransformedMouse();
  
  offscreen.beginDraw();
  
  
  offscreen.background(255);

  // We must always step through time!
  box2d.step();
  // Always alert the spring to the new mouse location
  spring.update(mouseX, mouseY);

  if (debug)
  {
    // Display all the boundaries
    for (Boundary wall : boundariesBottom) {
      wall.display();
    }
  }
  if (savePDF == true) {
    beginRecord(PDF, "BienQurbain" + frameCount + ".pdf");
  }

  // Display all the boxes
  for (Box b : boxes) {
    b.display();
  }

  if (dada && dadaCreated)
  {
    p.display();
    springPastille.update(mouseX, mouseY);
    if (p.theY() > height +150)
    {
      p.killBody();
      dadaCreated = false;
    }
  }

  if (recording) {
    saveFrame("output/frames####.png");
  }
  if (savePDF == true) {
    endRecord();
    savePDF = false;
  }

  // Boxes that leave the screen, we delete them
  // (note they have to be deleted from both the box2d world and our list

  for (int i = boxes.size()-1; i >= 0; i--) {
    Box b = boxes.get(i);
    if (b.done()) {
      boxes.remove(i);
    }
  }
  //println (boxes.size());

  if (boxes.size() == 0 && boundariesBottom.size()== 0 && dadaCreated == false)
  {
    createBoundariesBottom();
  }


  if (keysBU[0] == true &&  keysBU[1]== true)
  {
    buStarted = true;
    lastBUdropped = millis();
    indexCharBU = 0;
    keysBU[0] = false;
    keysBU[1] = false;
  }
  
  if (buStarted == true)
  {
   bu(); 
  }

  if (keysDADA[0] == true &&  keysDADA[1]== true && dadaCreated == false)
  {
    dada();
    keysDADA[0] = false;
    keysDADA[1] = false;
  }

  if (millis() - lastKeyPressed > pauseTime && artistsAlreadyCreated == false)
  {
    allArtists();
    buStarted = true;
    lastBUdropped = millis();
    indexCharBU = 0;
    artistsAlreadyCreated = true;
  }

  if (millis() - lastKeyPressed > pauseTime*2 && artistsAlreadyCreated == true)
  {
    while (boundariesBottom.size()>0)
    {
      boundariesBottom.get(boundariesBottom.size()-1).killBody();
      boundariesBottom.remove(boundariesBottom.size()-1);
      incrementalId = 0;
      lastKeyPressed = millis();
      artistsAlreadyCreated = false;
    }
  }
  offscreen.endDraw();
  background(0);
 
  // render the scene, transformed using the corner pin surface
  surface.render(offscreen);
}

// When the mouse is released we're done with the spring
void mouseReleased() {
  spring.destroy();
  if (dada && dadaCreated)
  {
    springPastille.destroy();
  }
  //box2d.setGravity(0, -20);
}

// When the mouse is pressed we. . .
void mousePressed() {
  for (int i = 0; i<boxes.size(); i++)
  {
    Box box = boxes.get(i);
    // Check to see if the mouse was clicked on the box
    if (box.contains(mouseX, mouseY)) {
      // And if so, bind the mouse location to the box with a spring
      spring.bind(mouseX, mouseY, box);
      selectedBox = box;
      selectedIndex = i;
    }
  }
  // GINKO
  if (dada && dadaCreated)
  {
    if (p.contains(mouseX, mouseY)) {
      springPastille.bind(mouseX, mouseY, p);
    }
  }
}





void allArtists()
{
  for (int i = 0; i<artists.size(); i++)
  {
    PShape MyPic = artists.get(i);
    Box p =  new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, i, -1, 0);
    boxes.add(p);
  }
}

void oneArtist(int index)
{

  PShape MyPic = artists.get(index);
  Box p =  new Box(MyPic.width/theScale, MyPic.height/theScale, MyPic, index, -1, 0);
  boxes.add(p);
}

void dada()
{
  p = new Pastille(MyPicDada.width/theScale, MyPicDada, 0);
  dadaCreated = true;
}


void bu()
{
  if (millis() - lastBUdropped > 300)
    {
      createLetter(buArray[indexCharBU]);
      println (buArray[indexCharBU] + "dropped at "+ lastBUdropped);
      lastBUdropped = millis();
      indexCharBU ++;
      if (indexCharBU == buArray.length)
      {
        buStarted = false;
      }
    }
}

void keyPressed() {
  lastKeyPressed = millis();
  artistsAlreadyCreated = true;
  int keyIndex = -1;
  if ( key >= 'a' && key <= 'z' && shift == false) {
    keyIndex = key - (97-65)  ;
    println (int(key), keyIndex);
    createLetter(keyIndex);
    if (incrementalIndexColor%3 == 0)
    {
      oneArtist(incrementalIndexArtists);
      if (incrementalIndexArtists < artists.size()-1)
      {
        incrementalIndexArtists ++;
      } else {
        incrementalIndexArtists = 0;
      }
    }
  } else 
  if (key == ' ')
  {
    if (boundariesBottom.size()>0)
    {
      boundariesBottom.get(boundariesBottom.size()-1).killBody();
      boundariesBottom.remove(boundariesBottom.size()-1);
      incrementalId = 0;
    }
  }
  if (key == CODED && keyCode == UP)shift = true;
  if (key == 'b' && shift == true)keysBU[0] = true;
  if (key == 'u' && shift == true)keysBU[1] = true;
  if (key == 'd' && shift == true)keysDADA[0] = true;
  if (key == 'a' && shift == true)keysDADA[1] = true;
  if (key == CODED && keyCode == DOWN)shiftBottom = true;
  if (key == 'c' && shiftBottom == true)  ks.toggleCalibration();
  if (key == 's' && shiftBottom == true) ks.load();
  if (key == 'l' && shiftBottom == true) ks.save();
    
  
}


void keyReleased()
{
  if (key == CODED && keyCode == UP)shift = false;
  if (key == CODED && keyCode == DOWN)shiftBottom = false;
  if (key == 'b')keysBU[0] = false;
  if (key == 'u')keysBU[1] = false;
  if (key == 'd' && shift == true)keysDADA[0] = false;
  if (key == 'a' && shift == true)keysDADA[1] = false;
} 

void createLetter(int keyIndex)
{
  if (millis()-lastLetter> delayBetweenDelay)
  {
    Box p = new Box(170/theScale, 245/theScale, null, incrementalId, keyIndex, colors[incrementalIndexColor%colors.length]);
    println (keyIndex);
    boxes.add(p);
    incrementalIndexColor++;
    incrementalId ++;

    lastLetter = millis();
  }
}

// This function returns all the files in a directory as an array of Strings  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    //Arrays.sort(names);
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

void shuffleArray(String[] array) {

  // with code from WikiPedia; Fisher–Yates shuffle 
  //@ <a href="http://en.wikipedia.org/wiki/Fisher" target="_blank" rel="nofollow">http://en.wikipedia.org/wiki/Fisher</a>–Yates_shuffle

  Random rng = new Random();

  // i is the number of items remaining to be shuffled.
  for (int i = array.length; i > 1; i--) {

    // Pick a random element to swap with the i-th element.
    int j = rng.nextInt(i);  // 0 <= j <= i-1 (0-based array)

    // Swap array elements.
    String tmp = array[j];
    array[j] = array[i-1];
    array[i-1] = tmp;
  }
}

void createBoundariesBottom()
{
  // Création de boundaries pour la trappe
  for (int i = 0; i<trapDiv; i++)
  {
    int widthBoundary = (width/trapDiv)+1;
    int originX = widthBoundary*i + (widthBoundary/2);
    int originY = height-13;
    boundariesBottom.add(new Boundary(originX, originY, widthBoundary, 25));
  }
  Collections.shuffle(boundariesBottom);
}